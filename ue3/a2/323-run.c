//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <semaphore.h>

#define ENV_MAX 4096

//sem_t semvar;

int main(int argc, char *argv[])
{
	FILE *f;
	//sem_init(&semvar, 0, 1);

	// get the environment variables we want to pass to the server
	uid_t uid = getuid();
	gid_t gid = getgid();
	char cwd[PATH_MAX];
	mode_t mask = umask(S_IWGRP | S_IWOTH);

	// get the current working directory...
	if (getcwd(cwd, PATH_MAX)!=NULL) {
		// ... and wait for the semaphore
		//sem_wait(&semvar);
		// open the named pipe ...
		f = fopen("/tmp/mynamedpipe", "w");
		if ((flock(fileno(f), LOCK_EX)) != -1) {
			printf("No filelock, writing to pipe.\n");
			// ... and print our data and the command we gave to the pipe
			fprintf(f, "%i %i %s %i\n", uid, gid, cwd, mask);
			for (int i = 1; i < argc; i++) {
				fprintf(f, "%s ", argv[i]);
			}
			fprintf(f, "\n");
		}
		flock(fileno(f), LOCK_UN);
		//sem_post(&semvar);
	} else {
		perror("getcwd");
		//sem_destroy(&semvar);
		exit(EXIT_FAILURE);
	}
	//sem_destroy(&semvar);

	exit(EXIT_SUCCESS);
}
