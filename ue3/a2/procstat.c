//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//

#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "procstat.h"

// read the file /proc/$pid/stat and parse its content
// (it does not parse all of the fields, only up to and
// including the signal fields)
void procpidstat(pid_t pid, procpidstat_t * ppstat) {
	char fn[PATH_MAX], szStatStr[2048], *datastart, *namestart;
	snprintf(fn, PATH_MAX, "/proc/%i/stat", pid);
	int fp;

	if ((fp = open(fn, O_RDONLY)) != -1) {
		if ((read(fp, szStatStr, 2048)) != -1) {

			// copy the pid
			sscanf(szStatStr, "%u", &(ppstat->pid));
			// calculate where the filanem starts and where the rest of the fields start
			namestart = strchr (szStatStr, '(') + 1;
			datastart = strrchr(szStatStr, ')');
			strncpy (ppstat->tcomm, namestart, datastart - namestart);
			ppstat->tcomm[datastart - namestart] = '\0';

			// copy the rest of the fields into the struct (there are mor fields, but we ignore them...)
			sscanf(datastart + 2,
			"%c %ld %ld %ld %ld %ld"
			"%lu %lu %lu %lu %lu %lu %lu"
			"%ld %ld %ld %ld %ld"
			"%lu %lu %lu %lu %lu %lu"
			"%lu %lu %lu %lu %lu %lu"
			"%lu %lu 0 0 0 ",
			&(ppstat->state), &(ppstat->ppid), &(ppstat->pgrp), &(ppstat->sid), &(ppstat->tty_nr), &(ppstat->tty_pgrp),
			&(ppstat->flags), &(ppstat->min_flt), &(ppstat->cmin_flt), &(ppstat->maj_flt), &(ppstat->cmaj_flt), &(ppstat->utime), &(ppstat->stime),
			&(ppstat->cutime), &(ppstat->cstime), &(ppstat->priority), &(ppstat->nice), &(ppstat->num_threads),
			&(ppstat->it_real_value), &(ppstat->start_time), &(ppstat->vsize), &(ppstat->rss), &(ppstat->rsslim), &(ppstat->start_code),
			&(ppstat->end_code), &(ppstat->start_stack), &(ppstat->esp), &(ppstat->eip), &(ppstat->pending), &(ppstat->blocked),
			&(ppstat->sigignore), &(ppstat->sigcatch));
		}
		close(fp);
	}
}
