//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//

#include <stdlib.h> //EXIT_SUCCESS, EXIT_FAILURE
#include <unistd.h> //getcwd()
#include <limits.h> //PATH_MAX
#include <stdio.h> //printf
#include <string.h> //strtok
#include <sys/wait.h> //waitpid
#include <signal.h> //signals
#include <setjmp.h> //sigsetjmp, siglongjmp
#include <errno.h> //errno
#include <pthread.h> //pthread_create
#include "procstat.h"
#include "rawio.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define LEN_MAX 1024
#define MAX_WORDS 128
#define PROMPT "323"
#define DELIM " \t\n"

#define yellow(x) printf("%s%s%s\n", ANSI_COLOR_YELLOW, x, ANSI_COLOR_RESET);
#define red(x) printf("%s%s%s\n", ANSI_COLOR_RED, x, ANSI_COLOR_RESET);
#define blue(x) printf("%s%s%s\n", ANSI_COLOR_BLUE, x, ANSI_COLOR_RESET);
#define magenta(x) printf("%s%s%s\n", ANSI_COLOR_MAGENTA, x, ANSI_COLOR_RESET);


char cwd[PATH_MAX];
int background = 0;
int numberofbg = 0;
int active = 1;
int lines = 0;
int promptlen = 0;

// extern char **environ; is POSIX standard. This signature for main is completely nonstandard, according to a
// comment on https://stackoverflow.com/questions/4291080/print-the-environment-variables-using-environ#4291100
extern char **environ;
extern int errno;

static sigjmp_buf env;
static volatile sig_atomic_t sigint_jump_active = 0;
// the mutex for the child counter
static pthread_mutex_t mutex;

// catch the signals we define in main()
void catch_sigint(int signo) {
	if (!sigint_jump_active) return;
	siglongjmp(env, 42);
}
void catch_sigquit(int signo) {
	if (!sigint_jump_active) return;
	siglongjmp(env, 42);
}
// catch the sigchild: whenever a child exits, lock the counter mutex and
// decrement the number of children
void catch_sigchld(int signo) {
	pthread_mutex_lock(&mutex);
	numberofbg--;
	pthread_mutex_unlock(&mutex);
}

// provide an overview of some useful information
// about the process
void getinfo()
{
	yellow("Effective User ID:");
	uid_t euid = geteuid();
	printf("%lu\n", (unsigned long int)euid);
	
	yellow("User ID:");
	uid_t uid = getuid();
	printf("%lu\n", (unsigned long int)uid);

	red("Environment:");
	int i = 0;
	while(environ[i]) {
		  printf("%s\n", environ[i++]); // prints in form of "variable=value"
	}

	magenta("Working Directory:");
	printf("%s\n", cwd);

	blue("/proc/$pid/stat Info:");
	pid_t pid = getpid();
	procpidstat_t pps;
	procpidstat(pid, &pps);
	printf("filename of the executable:\t%s\n", pps.tcomm);
	printf("state:\t%c\n", pps.state);
	printf("process id of the parent process:\t%lu\n", pps.ppid);
	unsigned long f;
	for (int i = 0; i < 8; i++) {
		f = pps.pending << i;
		if (pps.pending & f) {
			printf("%s (%i) is on the pending list.\n", strsignal(i), i);
		}
	}
	for (int i = 0; i < 8; i++) {
		f = pps.blocked << i;
		if (pps.blocked & f) {
			printf("%s (%i) is on the blocked list.\n", strsignal(i), i);
		}
	}
	for (int i = 0; i < 8; i++) {
		f = pps.sigignore << i;
		if (pps.sigignore & f) {
			printf("%s (%i) is on the ignore list.\n", strsignal(i), i);
		}
	}
	for (int i = 0; i < 8; i++) {
		f = pps.sigcatch << i;
		if (pps.sigcatch & f) {
			printf("%s (%i) is on the catch list.\n", strsignal(i), i);
		}
	}
}

// check if the string starts with an ampersand
int isbackground(char *str)
{
	return (str[0] == '&');
}

// split the given string at the delimiters and store the
// result in the cmdv array; return the length of the array
int split(char *str, char *delim, char **cmdv)
{
	int i = 0;
	for (cmdv[i] = strtok(str, delim); cmdv[i]; ++i, cmdv[i] = strtok(NULL, delim));
	return i;
}

// check the input string against builtin commands
// and execute them
// if the input string does not contain a builtin
// command, return 0
int builtin(int len, char **str)
{
	if (strcmp(str[0], "323-wo") == 0) {
		printf("%s\n", cwd);
		return 1;
	}
	if (strcmp(str[0], "cd") == 0) {
		if (len == 1) {
			if (chdir(getenv("HOME")) != 0) perror("cd");
		} else {
			if (chdir(str[1]) != 0) perror("cd");
		}
		return 1;
	}
	if (strcmp(str[0], "323-ende") == 0) {
		exit(EXIT_SUCCESS);
		return 1;
	}
	if (strcmp(str[0], "323-info") == 0) {
		getinfo();
		return 1;
	}
	if (strcmp(str[0], "323-setpath") == 0) {
		if (setenv("PATH", str[1], 1) != 0) perror("Setenv");
		return 1;
	}
	if (strcmp(str[0], "323-addtopath") == 0) {
		char buf[PATH_MAX];
		snprintf(buf, PATH_MAX, "%s:%s", getenv("PATH"), str[1]);
		if (setenv("PATH", buf, 1) != 0) perror("Setenv");
		return 1;
	}
	return 0;
}

// show a prompt
void prompt() {
	if (getcwd(cwd, PATH_MAX)!=NULL) {
		char prompt[PATH_MAX+6];
		snprintf(prompt, PATH_MAX+6, "%s-%s> ", PROMPT, cwd);
		promptlen = strlen(prompt);
		writestr_raw(prompt, 0, lines);
		//("%s@%s-%s> ", getenv("USER"), PROMPT, cwd);
	} else {
		perror("getcwd");
		exit(EXIT_FAILURE);
	}
}

// show some information in the upper right corner
void *prinfo(void *vargp)
{

	for (;;) {
		if (active) {

			time_t rawtime;
			struct tm * timeinfo;

			time(&rawtime);
			timeinfo = localtime(&rawtime);
			char infostr[LEN_MAX];
			snprintf(infostr, LEN_MAX, "BGProc: %i | Time: %s", numberofbg, asctime(timeinfo));
			writestr_raw(infostr, 25, 0);
		}
		sleep(1);
	}
}

// run the shell
void shell()
{
	char input[LEN_MAX];
	char *cmdv[MAX_WORDS];
	pid_t child;
	int statusPtr;
	pthread_t thread;
	lines = get_lines();


	pthread_create(&thread, NULL, prinfo, NULL);
	// the signals jump here to redraw
	// the prompt
	if (sigsetjmp(env, 1) == 42) {
		printf("\n");
	}
	sigint_jump_active = 1;
	clearscr();
	prompt();
	//while (fgets(input, LEN_MAX, stdin)!=NULL) {
	while (gets_raw(input, LEN_MAX, promptlen, lines)!=NULL) {
		// check if there even has been an input
		if (input[0] == 0) {
			continue;
		}
		printf("\n");
		// test if the input starts with an ampersand
		// and set the global background variable
		// accordingly. also replace the ampersand with
		// a space
		if (isbackground(input)) {
			background = 1;
			input[0] = ' ';
		}
		// split the input and store the length of
		// the resulting array `cmdv` in `len`
		int len = split(input, DELIM, cmdv);
		// pass the input array to the `builtin` function
		// and only if that does not detect a builtin command
		// go on with forking
		if (!builtin(len, cmdv)) {
			switch(child=fork()) {
				case -1: perror("Fork");
					 break;
				case 0: if (!background) {
						// reset the signals to default if its not a background task
						if (signal(SIGINT, SIG_DFL) == SIG_ERR) perror("Signal");
						if (signal(SIGQUIT, SIG_DFL) == SIG_ERR) perror("Signal");
					} else {
						// ignore the signals if its a background task
						if (signal(SIGINT, SIG_IGN) == SIG_ERR) perror("Signal");
						if (signal(SIGQUIT, SIG_IGN) == SIG_ERR) perror("Signal");
						//printf("Sending to background...\n");
					}
					execvp(cmdv[0], cmdv);
					perror("Execvp");
					exit(EXIT_FAILURE);
				default:
					// lock the counter mutex and increment the number of clients
					pthread_mutex_lock(&mutex);
					numberofbg++;
					pthread_mutex_unlock(&mutex);
					if (background) {
						background = 0;
					} else {
						// if our forked process is not a background task, wait for
						// it in the foreground
						active = 0;
						if (waitpid(child, &statusPtr, 0) == -1) perror("Waitpid");
						active = 1;
					}
					break;
			}
		}
		prompt();
	}
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	if (signal(SIGINT, catch_sigint) == SIG_ERR) perror("Signal");
	if (signal(SIGQUIT, catch_sigquit) == SIG_ERR) perror("Signal");
	if (signal(SIGCHLD, catch_sigchld) == SIG_ERR) perror("Signal");

	shell();

	exit(EXIT_SUCCESS);
}
