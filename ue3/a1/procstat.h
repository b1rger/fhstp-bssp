//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//

#ifndef PROCSTAT_H_
#define PROCSTAT_H_

#include <limits.h>

// see also https://github.com/torvalds/linux/blob/master/fs/proc/array.c#L528
// and https://www.kernel.org/doc/Documentation/filesystems/proc.txt */
typedef struct statstruct_proc {
  int           pid;			/** %d The process id. **/
  char          tcomm[PATH_MAX];	/** %s The filename of the executable **/

  char          state;			/** %c R is running, S is sleeping, D is sleeping in an uninterruptible wait, Z is zombie, T is traced or stopped **/
  long		ppid;			/** %d The pid of the parent. **/
  long		pgrp;			/** %d The pgrp of the process. **/
  long		sid;			/** %d The session id of the process. **/
  long		tty_nr;			/** %d The tty the process uses **/
  long		tty_pgrp;		/** %d  **/

  unsigned long	flags;			/** %lu The flags of the process. **/
  unsigned long	min_flt;		/** %lu The number of minor faults **/
  unsigned long	cmin_flt;		/** %lu The number of minor faults with childs **/
  unsigned long	maj_flt;		/** %lu The number of major faults **/
  unsigned long cmaj_flt;		/** %lu The number of major faults with childs **/
  unsigned long utime;			/** %lu user mode jiffies **/
  unsigned long stime;			/** %lu kernel mode jiffies **/

  long		cutime;			/** %ld user mode jiffies with childs **/
  long		cstime;			/** %ld kernel mode jiffies with childs **/
  long		priority;		/** %ld the standard nice value, plus fifteen **/
  long		nice;
  long		num_threads;

  unsigned long it_real_value;		/* always zero */
  unsigned long start_time;		/** %ld Time the process started after system boot **/
  unsigned long vsize;			/** %lu Virtual memory size **/
  unsigned long rss;
  unsigned long rsslim;			/** %lu Resident Set Size **/
  unsigned long start_code;		/** %lu The address above which program text can run **/
  unsigned long	end_code;		/** %lu The address below which program text can run **/
  unsigned long start_stack;		/** %lu The address of the start of the stack **/
  unsigned long esp;			/** %lu The current value of ESP **/
  unsigned long eip;			/** %lu The current value of EIP **/
  unsigned long	pending;		/** %lu The bitmap of pending signals **/
  unsigned long blocked;		/** %lu The bitmap of blocked signals **/
  unsigned long sigignore;		/** %lu The bitmap of ignored signals **/
  unsigned long sigcatch;		/** %lu The bitmap of catched signals **/
} procpidstat_t;

void procpidstat(pid_t pid, procpidstat_t *ppstat);

#endif
