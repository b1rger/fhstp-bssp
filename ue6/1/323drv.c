#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Birger Schacht");
MODULE_DESCRIPTION("A simple module to demonstrate loading and unloading.");

static int __init is323drv_init(void)
{
	printk(KERN_INFO "is323drv: Hello, kernel world!\n");
	return 0;
}
static void __exit is323drv_exit(void)
{
	printk(KERN_INFO "is323drv: Goodbye, kernel world!\n");
}

// Driver initialization entry point
// Function to be run at kernel boot time or module insertion.
module_init(is323drv_init);
// Driver exit entry point
// Function to be run when driver is removed.
module_exit(is323drv_exit);
