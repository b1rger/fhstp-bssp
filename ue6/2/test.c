#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	char *wbuf = "hello world";
	char rbuf[1024];

	int fp = open("/dev/is323drv0", O_RDWR);
	read(fp, rbuf, 1024);
	write(fp, wbuf, strlen(wbuf));
	close(fp);

	exit(EXIT_SUCCESS);

}
