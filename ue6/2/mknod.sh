#!/bin/sh

device="is323drv"
rm -f /dev/${device}[01234]

major=$(grep is323 /proc/devices | cut -f 1 -d " ")
echo "Major number is $major"

for i in `seq 0 4`; do
	mknod /dev/${device}${i} c $major ${i}
done
