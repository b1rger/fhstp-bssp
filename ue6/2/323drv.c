#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/errno.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Birger Schacht");
MODULE_DESCRIPTION("A simple module to demonstrate loading and unloading.");

#define MINOR_COUNT 5
#define DRVNAME "is323drv"

static int is323drv_open(struct inode *inode, struct file *filp);
static ssize_t is323drv_read(struct file *filp, char __user *buff, size_t count, loff_t *offset);
static ssize_t is323drv_write(struct file *filp, const char __user *userp, size_t size, loff_t *offset);
static int is323drv_release(struct inode *inode, struct file *filp);

// the file operations we implement
static struct file_operations is323drv_fcalls = {
	.owner	 = THIS_MODULE,
	.open	 = is323drv_open,
	.release = is323drv_release,
	.read	 = is323drv_read,
	.write	 = is323drv_write,
};

// a variable to store major and minor number
static dev_t dev_num;

// for every device we create a character device struct
// that contains at least the cdev variable and maybe some
// other attributes
struct is323drv_cdev
{
	//int foo;
	struct cdev chdev;
};

// we store all the devices in a static array
static struct is323drv_cdev is323drv_devs[MINOR_COUNT];

// the initialization function- it gets called when the module
// gets loaded
static int __init is323drv_init(void)
{
	int rv;
	int i, ii;

	// a debug message to inform us that the module got loaded
	printk(KERN_INFO "Hello from my character driver %s!\n", DRVNAME);

	// register a range of device numbers and store the first assigned number
	// in dev_num
	rv = alloc_chrdev_region(&dev_num, 0, MINOR_COUNT, DRVNAME);
	if (rv) {
		goto err1;
	}

	// print the major and minor numbers for debugging purposes
	printk(KERN_INFO "major nr: %d, start with minor nr: %d\n", MAJOR(dev_num), MINOR(dev_num));

	// create the devices
	for (i = 0; i < MINOR_COUNT; ++i) {
		// create new dev_t numbers ...
		dev_t cur_devnr = MKDEV(MAJOR(dev_num), MINOR(dev_num) + i);
		// ... and initialize a cdev struct
		cdev_init(&is323drv_devs[i].chdev, &is323drv_fcalls);
		// do we really need this?
		//is323drv_devs[i].chdev.owner = THIS_MODULE;

		// add the character devive to the system with the current dev_t
		rv = cdev_add(&is323drv_devs[i].chdev, cur_devnr, 1);
		if (rv < 0) {
			printk(KERN_WARNING "cdev_add failed\n");
			goto err2;
		}
		printk(KERN_INFO "new device with major nr: %d, minor nr: %d\n", MAJOR(cur_devnr), MINOR(cur_devnr));
	}
	return 0;
err2:
	// clean up
	for (ii = 0; ii < i; ++ii) {
		cdev_del(&is323drv_devs[ii].chdev);
	}
	unregister_chrdev_region(dev_num, MINOR_COUNT);
err1:
	return rv;
}

static void __exit is323drv_exit(void)
{
	int i;
	// iterate through our devices and delete them using
	// cdev_del (This guarantees that cdev device will no longer be able to
	// be opened, however any cdevs already open will remain and their fops
	// will still be callable even after cdev_del returns)
	for (i = 0; i < MINOR_COUNT; i++) {
		cdev_del(&is323drv_devs[i].chdev);
		printk("cleanup device %d\n", i);
	}
	// This function will unregister a range of count device numbers,
	// starting with from. The caller should normally be the one who
	// allocated those numbers in the first place...
	unregister_chrdev_region(dev_num, MINOR_COUNT);

	printk(KERN_INFO "Remove my character driver %s", DRVNAME);
}

static int is323drv_open(struct inode *inode, struct file *filp) {
	// a debugging message when our device gets opened
	printk(KERN_INFO "is323drv_open() called\n");
	return 0;
}

static ssize_t is323drv_read(struct file *filp, char __user *buff, size_t count, loff_t *offset) {
	// a debugging message when our device gets read from
	printk(KERN_INFO "is323drv_read() called; bytes: %zu\n", count);
	return 0;
}

static ssize_t is323drv_write(struct file *filp, const char __user *user, size_t size, loff_t *offset) {
	// a debugging message when our device gets written to
	printk(KERN_INFO "is323drv_write() called; bytes: %zu\n", size);
	return size;
}

static int is323drv_release(struct inode *inode, struct file *filp) {
	// a debugging message when our device gets closed
	printk(KERN_INFO "is323drv_release() called\n");
	return 0;
}

module_init(is323drv_init);
module_exit(is323drv_exit);

