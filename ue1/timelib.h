#ifndef TIMELIB_H_
#define TIMELIB_H_
#include <stdio.h>
#include <time.h>

#define BILLION 1000000000

struct timespec get_cur_time_323();
void write_time_323(const char *msg, const struct timespec *t);
struct timespec get_diff_323(const struct timespec *a, const struct timespec *b);
void add_time_323(struct timespec *a, const struct timespec *b);

#endif
