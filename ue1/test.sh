#!/bin/bash

BUFSIZES='10 100 1000 10000'
THREADS='2 4 6'

mkdir -p testdata/
blocksize=100000
count=1000
for i in `seq 1 4`; do
	dd if=/dev/urandom of=testdata/${count}-${blocksize}-${i}.raw bs=${blocksize} count=${count} status=none
done

for BUFSIZE in ${BUFSIZES}; do
	make ${BUFSIZE}
	for i in ${THREADS}; do
		time ./${BUFSIZE} testdata ${i} > /dev/null
	done
done
