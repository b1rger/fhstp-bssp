#include "timelib.h"

struct timespec get_cur_time_323()
{
	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);
	return spec;
}

struct timespec get_diff_323(const struct timespec *beginning, const struct timespec *end)
{
	struct timespec diff;
	diff.tv_sec = difftime(end->tv_sec, beginning->tv_sec);
	if (end->tv_nsec >= beginning->tv_nsec) {
		diff.tv_nsec = end->tv_nsec - beginning->tv_nsec;
	} else {
		diff.tv_nsec = BILLION - (beginning->tv_nsec - end->tv_nsec);
		diff.tv_sec--;
	}
	return diff;
}

void write_time_323(const char *msg, const struct timespec *t)
{
	fprintf(stderr, "%s\t%lds,\t%ldns\n", msg, t->tv_sec, t->tv_nsec);
}

void add_time_323(struct timespec *sum, const struct timespec *addtime)
{
	sum->tv_sec += addtime->tv_sec;
	sum->tv_nsec += addtime->tv_nsec;
	if (sum->tv_nsec >= BILLION) {
		sum->tv_nsec -= BILLION;
		sum->tv_sec++;
	}
}
