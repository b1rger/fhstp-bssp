#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/ioctl.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/completion.h>
#include "323_ioctl.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Birger Schacht");
MODULE_DESCRIPTION("A simple module to demonstrate loading and unloading.");

#define MINOR_COUNT 5
#define DRVNAME "is323"
#define BUF_SIZE 512
#define PROC_FILE "info"
#define PROC_DIR DRVNAME

static int is323drv_open(struct inode *inode, struct file *filp);
static ssize_t is323drv_read(struct file *filp, char __user *buff, size_t count, loff_t *offset);
static ssize_t is323drv_write(struct file *filp, const char __user *userp, size_t size, loff_t *offset);
static int is323drv_release(struct inode *inode, struct file *filp);
static long is323drv_ioctl(struct file *filep,unsigned int cmd, unsigned long arg);
void *is323_start(struct seq_file *sf, loff_t *pos);
void *is323_next(struct seq_file *sf, void *it, loff_t *pos);
void is323_stop(struct seq_file *sf, void *it);
int is323_show(struct seq_file *sf, void *it);
static int is323drv_seq_open(struct inode *inode, struct file *filp);

// the file operations we implement
static struct file_operations is323drv_fcalls = {
	.owner	 = THIS_MODULE,
	.open	 = is323drv_open,
	.release = is323drv_release,
	.read	 = is323drv_read,
	.write	 = is323drv_write,
	.unlocked_ioctl = is323drv_ioctl,
};
static struct seq_operations is323_seq_ops = {
	.start	= is323_start,
	.next	= is323_next,
	.stop	= is323_stop,
	.show	= is323_show
};

static struct file_operations is323drv_proc_fcalls = {
	.owner	 = THIS_MODULE,
	.open	= is323drv_seq_open,
	.read	= seq_read,
	.llseek = seq_lseek,
	.release= seq_release,
};

// a variable to store major and minor number
static dev_t dev_num;

static struct class *is323_class;

// for every device we create a character device struct
// that contains at least the cdev variable and maybe some
// other attributes
struct is323drv_cdev
{
	struct cdev chdev;
	int read_index;
	int write_index;
	int currentbytes;
	char *buffer;
	struct semaphore sync;
	int r;
	int w;
	struct completion compl;
};

// we store all the devices in a static array
static struct is323drv_cdev is323drv_devs[MINOR_COUNT];

static struct proc_dir_entry *parent;
static struct proc_dir_entry *entry;

// the initialization function- it gets called when the module
// gets loaded
static int __init is323drv_init(void)
{
	int rv, i, ii;

	// a debug message to inform us that the module got loaded
	printk(KERN_INFO "Hello from my character driver %s!\n", DRVNAME);

	// register a range of device numbers and store the first assigned number
	// in dev_num
	rv = alloc_chrdev_region(&dev_num, 0, MINOR_COUNT, DRVNAME);
	if (rv) {
		goto err1;
	}

	is323_class = class_create(THIS_MODULE, "is323");
	if (IS_ERR(is323_class)) {
		rv = -EACCES;
		goto err1b;
	}
	parent = proc_mkdir(PROC_DIR, NULL);
	if (IS_ERR(parent)) {
		rv = -EACCES;
		goto err1b;
	}
	entry = proc_create_data(PROC_FILE, 0666, parent, &is323drv_proc_fcalls, NULL);
	if (IS_ERR(entry)) {
		rv = -EACCES;
		goto err1b;
	}

	// print the major and minor numbers for debugging purposes
	printk(KERN_INFO "major nr: %d, start with minor nr: %d\n", MAJOR(dev_num), MINOR(dev_num));

	// create the devices
	for (i = 0; i < MINOR_COUNT; ++i) {
		// create new dev_t numbers ...
		dev_t cur_devnr = MKDEV(MAJOR(dev_num), MINOR(dev_num) + i);
		// ... and initialize a cdev struct
		cdev_init(&is323drv_devs[i].chdev, &is323drv_fcalls);

		if (IS_ERR(device_create(is323_class, NULL, cur_devnr, NULL, "is323%d", i))) {
			rv = -ENODEV;
			goto err2;
		}

		// add the character devive to the system with the current dev_t
		rv = cdev_add(&is323drv_devs[i].chdev, cur_devnr, 1);

		if (rv < 0) {
			printk(KERN_WARNING "cdev_add failed\n");
			device_destroy(is323_class, is323drv_devs[i].chdev.dev);
			goto err2;
		}
		sema_init(&is323drv_devs[i].sync, 1);
		is323drv_devs[i].buffer = NULL;
		is323drv_devs[i].write_index = 0;
		is323drv_devs[i].read_index = 0;
		is323drv_devs[i].currentbytes = 0;
		is323drv_devs[i].r = 0;
		is323drv_devs[i].w = 0;
		init_completion(&is323drv_devs[i].compl);
		printk(KERN_INFO "new device with major nr: %d, minor nr: %d\n", MAJOR(cur_devnr), MINOR(cur_devnr));
	}
	return 0;
err2:
	// clean up
	for (ii = 0; ii < i; ++ii) {
		cdev_del(&is323drv_devs[ii].chdev);
		device_destroy(is323_class, is323drv_devs[i].chdev.dev);
	}
	class_destroy(is323_class);
err1b:
	unregister_chrdev_region(dev_num, MINOR_COUNT);
err1:
	return rv;
}

static void __exit is323drv_exit(void)
{
	int i;
	remove_proc_entry(PROC_FILE, parent);
	remove_proc_entry(PROC_DIR, NULL);
	// iterate through our devices and delete them using
	// cdev_del (This guarantees that cdev device will no longer be able to
	// be opened, however any cdevs already open will remain and their fops
	// will still be callable even after cdev_del returns)
	for (i = 0; i < MINOR_COUNT; i++) {
		cdev_del(&is323drv_devs[i].chdev);
		device_destroy(is323_class, is323drv_devs[i].chdev.dev);
		if(is323drv_devs[i].buffer) {
			kfree(is323drv_devs[i].buffer);
		}
		printk("cleanup device %d\n", i);
	}
	class_destroy(is323_class);
	// This function will unregister a range of count device numbers,
	// starting with from. The caller should normally be the one who
	// allocated those numbers in the first place...
	unregister_chrdev_region(dev_num, MINOR_COUNT);

	printk(KERN_INFO "Remove my character driver %s", DRVNAME);
}

static int is323drv_open(struct inode *inode, struct file *filp) {
	struct is323drv_cdev *dev; /* device information */
	int ret;
	dev = container_of(inode->i_cdev, struct is323drv_cdev, chdev);;
	filp->private_data = dev;
	ret = 0;

	if(down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	if ((filp->f_mode & FMODE_READ) && !(filp->f_mode & FMODE_WRITE)) {
		if (filp->f_flags & O_NONBLOCK) {
			if (!dev->w) {
				ret = -EAGAIN;
				goto err;
			}
		} else {
			up(&dev->sync);
			wait_for_completion(&dev->compl);
			if(down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}

		}
	}

	if (filp->f_mode & FMODE_READ) {
		dev->r++;
	}

	if (filp->f_mode & FMODE_WRITE) {
		dev->w++;
		complete(&dev->compl);
	}

	if (!dev->buffer)
		dev->buffer = kmalloc(BUF_SIZE * sizeof(char *), GFP_USER);

err:
	up(&dev->sync);
	return ret;
}

static ssize_t is323drv_read(struct file *filp, char __user *buf, size_t size, loff_t *offset) {
	struct is323drv_cdev *dev = filp->private_data;
	int bytesread, i, nextsize=0;

	if (!dev->currentbytes)
		return 0;

	printk(KERN_INFO "is323drv_read() called; bytes: %zu\n", size);
	if(down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	if (size > dev->currentbytes)
		size = dev->currentbytes;

	if (size > (BUF_SIZE - dev->read_index)) {
		nextsize = size - (BUF_SIZE - dev->read_index);
		size = BUF_SIZE - dev->read_index;
	}

	bytesread = size - copy_to_user(buf, dev->buffer + dev->read_index, size);
	if (nextsize) {
		bytesread += nextsize - copy_to_user(buf + size, dev->buffer, nextsize);
	}

	dev->read_index += bytesread;
	dev->read_index %= BUF_SIZE;
	dev->currentbytes -= bytesread;

	printk("Readindex %d, currentbytes: %d", dev->read_index, dev->currentbytes);
	for (i=0; i<BUF_SIZE; i++)
		printk("Pos %i: [%c]", i, dev->buffer[i]);

	up(&dev->sync);
	nextsize=0;
	return bytesread;
}

static ssize_t is323drv_write(struct file *filp, const char __user *buf, size_t size, loff_t *offset) {
	struct is323drv_cdev *dev = filp->private_data;
	int byteswritten, i, nextsize = 0;

	if (dev->currentbytes >= BUF_SIZE) {
		return ENOSPC;
	}

	printk(KERN_INFO "is323drv_write() called; bytes: %zu\n", size);
	if(down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	if (size > BUF_SIZE)
		size = BUF_SIZE;

	if (size > (BUF_SIZE - dev->currentbytes)) {
		size = BUF_SIZE - dev->currentbytes;
	}

	if (size > (BUF_SIZE - dev->write_index)) {
		nextsize = size - (BUF_SIZE - dev->write_index);
		size = BUF_SIZE - dev->write_index;
	}
	byteswritten = size - copy_from_user(dev->buffer + dev->write_index, buf, size);
	if (nextsize) {
		byteswritten+= nextsize - copy_from_user(dev->buffer, buf + size, nextsize);
	}

	dev->write_index += byteswritten;
	dev->write_index %= BUF_SIZE;
	dev->currentbytes += byteswritten;

	printk("Writeindex %d, currentbytes: %d", dev->write_index, dev->currentbytes);
	for (i=0; i<BUF_SIZE; i++)
		printk("Pos %i: [%c]", i, dev->buffer[i]);

	up(&dev->sync);
	nextsize=0;
	return byteswritten;
}

static int is323drv_release(struct inode *inode, struct file *filp) {
	struct is323drv_cdev *dev = filp->private_data;

	if (down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	if (filp->f_mode & FMODE_WRITE) {
		dev->w--;
	}
	if (filp->f_mode & FMODE_READ) {
		dev->r--;
	}

	up(&dev->sync);

	printk(KERN_INFO "is323drv_release() called\n");
	return 0;
}

static long is323drv_ioctl(struct file *filep, unsigned int cmd, unsigned long arg){
	long rv = -EINVAL;
	struct is323drv_cdev *dev = filep->private_data;
	printk(KERN_INFO "is323_ioctl() called\n");
	
	if(_IOC_TYPE(cmd) != IS323_IOC_MAGIC){
		return rv;
	}
	if(down_interruptible(&dev->sync)){
		return -ERESTARTSYS;
	}
	
	switch(_IOC_NR(cmd)){
		case IS323_IOC_NR_PIPEREADINDEX:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				rv = dev->read_index;
				break;
			}
			else
				break;
		case IS323_IOC_NR_PIPEWRITEINDEX:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				rv = dev->write_index;
				break;
			}
			else
				break;
		case IS323_IOC_NR_STORED:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				rv = dev->currentbytes;
				break;
			}
			else
				break;
		case IS323_IOC_NR_W:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				rv = dev->w;
				break;
			}
			else
				break;
		case IS323_IOC_NR_R:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				rv = dev->r;
				break;
			}
			else
				break;
		case IS323_IOC_NR_WIPE:
			if(_IOC_DIR(cmd) == _IOC_NONE) {
				dev->currentbytes = 0;
				dev->write_index = 0;
				dev->read_index = 0;
				rv = dev->currentbytes;
				break;
			}
			else
				break;
		default:
			return rv;
	}
	
	up(&dev->sync);
	return rv;
}

void *is323_start(struct seq_file *sf, loff_t *pos)
{
	printk("start() %d\n", (int)*pos);
	if (*pos == 0)
		return is323drv_devs;
	return NULL;
}

void *is323_next(struct seq_file *sf, void *it, loff_t *pos)
{
	++(*pos);
	if (*pos >= MINOR_COUNT) {
		return NULL;
	}
	return &is323drv_devs[*pos];
	
}
void is323_stop(struct seq_file *sf, void *it)
{
}
int is323_show(struct seq_file *sf, void *it)
{
	struct is323drv_cdev *dev = it;
	int devid = (int)(dev - is323drv_devs);
	if (down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}
	seq_printf(sf, "Device %d: read_index %d\n", devid, dev->read_index);
	seq_printf(sf, "Device %d: write_index %d\n", devid, dev->write_index);
	seq_printf(sf, "Device %d: currentbytes %d\n", devid, dev->currentbytes);
	seq_printf(sf, "Device %d: r %d\n", devid, dev->r);
	seq_printf(sf, "Device %d: w %d\n", devid, dev->w);
	up(&dev->sync);
	return 0;
}
static int is323drv_seq_open(struct inode *inode, struct file *filp)
{
	printk("seq_open()");
	return seq_open(filp, &is323_seq_ops);
}

module_init(is323drv_init);
module_exit(is323drv_exit);

