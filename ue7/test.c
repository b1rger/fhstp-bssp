#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <errno.h>
#include "323_ioctl.h"

extern int errno;

int main(int argc, char *argv[])
{
	/* these tests expects the buffer to be 10 characters long */
	char writebuf[20];
	char readbuf[20];
	ssize_t bytesread, byteswritten;
	int fd;

	fd = open("/dev/is3230", O_RDONLY | O_NONBLOCK);
	assert(fd == -1);
	assert(errno == EAGAIN);
	close(fd);

	printf("echo foo > /dev/is3231 to unlock device\n");
	fd = open("/dev/is3231", O_RDONLY);
	close(fd);

	fd = open("/dev/is3232", O_RDWR | O_NONBLOCK);

	// make full
	strcpy(writebuf, "0123456789");
	byteswritten = write(fd, writebuf, 10);
	//printf("write(2) returned %i\n", byteswritten);
	assert(byteswritten == 10);
	bytesread = read(fd, readbuf, 20);
	assert(bytesread == 10);
	//printf("read(2) asked for 20, returned %i (%s)\n", bytesread, readbuf);
	assert(strncmp(readbuf, writebuf, 10) == 0);


	// write more than avail, read more than avail
	strcpy(writebuf, "01234567890ABCDEF");
	byteswritten = write(fd, writebuf, strlen(writebuf));
	//printf("write(2) returned %i\n", byteswritten);
	assert(byteswritten == 10);
	bytesread = read(fd, readbuf, 20);
	assert(bytesread == 10);
	assert(strncmp(readbuf, writebuf, 10) == 0);
	//printf("read(2) asked for 20, returned %i (%s)\n", bytesread, readbuf);

	// write two times half the buffer, read more than allowed
	strcpy(writebuf, "012345");
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 6);
	//printf("write(2) returned %i\n", byteswritten);
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 4);
	//printf("write(2) returned %i\n", byteswritten);
	bytesread = read(fd, readbuf, 10);
	assert(bytesread == 10);
	//printf("read(2) asked for 20, returned %i (%s)\n", bytesread, readbuf);

	// write half, read everything, write half, read everything
	strcpy(writebuf, "012345");
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 6);
	//printf("write(2) returned %i\n", byteswritten);
	bytesread = read(fd, readbuf, 10);
	assert(bytesread == 6);
	//printf("read(2) asked for 20, returned %i (%s)\n", bytesread, readbuf);
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 6);
	//printf("write(2) returned %i\n", byteswritten);
	bytesread = read(fd, readbuf, 10);
	assert(bytesread == 6);
	//printf("read(2) asked for 20, returned %i (%s)\n", bytesread, readbuf);
	
	strcpy(writebuf, "01234567");
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 8);
	bytesread = read(fd, readbuf, 5);
	assert(bytesread == 5);
	strcpy(writebuf, "A");
	byteswritten = write(fd, writebuf, strlen(writebuf));
	assert(byteswritten == 1);
	

	int readindex, writeindex, stored;

        readindex = ioctl(fd, IS323_IOC_PIPEREADINDEX);
	//printf("%i\n", readindex);
	assert(readindex == 7);

	writeindex = ioctl(fd, IS323_IOC_PIPEREADINDEX);
	//printf("%i\n", writeindex);
	assert(writeindex == 7);
	stored = ioctl(fd, IS323_IOC_STORED);
	//printf("%i\n", stored);
	assert(stored == 4);

	printf("wiping device /dev/is3232; should be empty in /proc/is323/info\n");
	ioctl(fd, IS323_IOC_WIPE);

	close(fd);
	exit(EXIT_SUCCESS);
}
