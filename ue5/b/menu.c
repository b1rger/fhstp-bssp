#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h> // ftrucate
#include <semaphore.h>
#include <fcntl.h>
#include <errno.h>
#include "323.h"

sem_t *semvarread;
sem_t *semvarwrite;
char *shm_base;
int shmid;

static void exit_handler(void) {
	printf("Exit handler: cleaning up...\n");
	/* remove the mapped memory segment from the address space of the process */
	if (munmap(shm_base, SHM_SIZE) == -1) {
		perror("munmap");
	}
	if (close(shmid) == -1) {
		perror("close");
	}
	if (shm_unlink(SHM) == -1) {
		perror("shm_unlink");
	}
	if (sem_destroy(semvarread) == -1) {
		perror("sem_destroy read");
	}
	if (sem_unlink(SEMVARREAD) == -1) {
		perror("sem_unlink read");
	}
	if (sem_destroy(semvarwrite) == -1) {
		perror("sem_destroy write");
	}
	if (sem_unlink(SEMVARWRITE) == -1) {
		perror("sem_unlink write");
	}
	_exit(EXIT_SUCCESS);
}

void sigint_handler(int signo)
{
	exit_handler();
}

int main(int argc, char *argv[])
{
	char input[MSG_SIZE];

	if (signal(SIGINT, sigint_handler) == SIG_ERR) perror("Signal");
	atexit(exit_handler);

	if ((shmid = shm_open(SHM, O_CREAT | O_RDWR, 0666))== -1) {
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	if (ftruncate(shmid, SHM_SIZE) == -1) {
		perror("ftrucate");
		exit(EXIT_FAILURE);
	}
	shm_base = mmap(0, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shmid, 0);
	if (shm_base == MAP_FAILED) {
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	if ((semvarread = sem_open(SEMVARREAD, O_CREAT | O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("semopen");
		exit(EXIT_FAILURE);
	}
	if ((semvarwrite = sem_open(SEMVARWRITE, O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("semopen");
		exit(EXIT_FAILURE);
	}

	while (fgets(input, MSG_SIZE, stdin)!=NULL) {
		sem_wait(semvarwrite);
		printf("addint go SHM %s", input);
		sprintf(shm_base, "%s", input);
		sem_post(semvarread);
		if (strcmp(input, QUIT) == 0) {
			break;
		}
	}
	

	exit(EXIT_SUCCESS);
}
