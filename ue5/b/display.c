#include <stdlib.h>
#include <mqueue.h>
#include <stdio.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/mman.h> //shm_open
#include <semaphore.h> // sem_t
#include <unistd.h>
#include "323.h"

sem_t *semvarread;
sem_t *semvarwrite;
int shmfd;
char *shm_base;

static void exit_handler(void) {
	printf("Exit handler: cleaning up...\n");
	if (munmap(shm_base, SHM_SIZE) == -1) {
		perror("munmap");
	}
	if (sem_destroy(semvarread) == -1) {
		perror("sem_destroy read");
	}
	if (sem_destroy(semvarwrite) == -1) {
		perror("sem_destroy write");
	}
	if (close(shmfd) == -1) {
		perror("close");
	}
	if (shm_unlink(SHM) == -1) {
		perror("shm_unlink");
	}

	_exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	atexit(exit_handler);


	shmfd = shm_open(SHM, O_RDONLY, 0666);
	if (shmfd == -1) {
		perror("shm_id");
		exit(EXIT_FAILURE);
	}
	/* map the shared memory segment to the address space of the process */
	shm_base = mmap(0, SHM_SIZE, PROT_READ, MAP_SHARED, shmfd, 0);
	if (shm_base == MAP_FAILED) {
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	if ((semvarread = sem_open(SEMVARREAD, O_RDWR)) == SEM_FAILED) {
		perror("semopen read");
		exit(EXIT_FAILURE);
	}
	if ((semvarwrite = sem_open(SEMVARWRITE, O_RDWR)) == SEM_FAILED) {
		perror("semopen write");
		exit(EXIT_FAILURE);
	}
	while(1) {
		sem_wait(semvarread);
		if (strcmp(shm_base, QUIT) == 0) {
			exit(EXIT_SUCCESS);
		}
		printf("%s", shm_base);
		sem_post(semvarwrite);
	}
	
	exit(EXIT_SUCCESS);
}
