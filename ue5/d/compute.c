#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <mqueue.h>
#include <string.h>
#include <limits.h> //PATH_MAX
#include <unistd.h> //unlink
#include <sys/stat.h> //umask
#include "323.h"

int toupp(char *msg) {
	while (*msg = toupper(*msg)) msg++;
}

int main(int argc, char *argv[])
{
	char input[MSG_SIZE];
	mqd_t mqdes;
	ssize_t received;
	char myfifo[PATH_MAX];
	int mkn;

	if ((mqdes = mq_open(QUEUE, O_RDONLY)) == -1) {
		perror("mq_open");
		exit(EXIT_FAILURE);
	}

	char *home = getenv("HOME");
	snprintf(myfifo, PATH_MAX, "%s/%s", home, FIFONAME);
	if (unlink(myfifo) == -1)
		perror("unlink");
	if ((mkn = mknod(myfifo, S_IFIFO|0666, 0)) == -1)
		perror("mknod");
	int fp = open(myfifo, O_WRONLY);

	while (1) {
		if ((received = mq_receive(mqdes, input, sizeof(input), NULL)) == -1) {
			perror("mq_receive");
			exit(EXIT_FAILURE);
		}

		printf("(got %s, forwarding to pipe)", input);
		input[received] = '\0';
		toupp(input);
		write(fp, input, strlen(input));

		if (strcmp(input, QUITUP) == 0)
			break;
	}

	if (close(fp) == -1)
		perror("close");

	if (unlink(myfifo) == -1)
		perror("unlink");

	exit(EXIT_SUCCESS);
}
