#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <signal.h>
#include "323.h"

mqd_t mqdes;

static void exit_handler(void) {
	printf("Exit hander: cleaning up...\n");
	mq_close(mqdes);
	if (mq_unlink(QUEUE) == -1)
		perror("mq_unlink");
	_exit(EXIT_SUCCESS);
}

void sigint_handler(int signo)
{
	exit_handler();
}



int main(int argc, char *argv[])
{
	struct mq_attr attr;
	char input[MSG_SIZE];
	size_t snt;

	if (signal(SIGINT, sigint_handler) == SIG_ERR) perror("Signal");
	atexit(exit_handler);

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = 10;
	attr.mq_msgsize = MSG_SIZE;
	attr.mq_curmsgs = 0;

	if (mq_unlink(QUEUE) == -1)
		perror("mq_unlink");
	
	mqdes = mq_open(QUEUE, O_RDWR | O_CREAT, 0644, &attr);


	while (fgets(input, MSG_SIZE, stdin)!=NULL) {
		printf(">>> %s", input);
		if ((snt = mq_send(mqdes, input, strlen(input), 1)) == -1)
			perror("mq_send");
		if (strcmp(input, QUIT) == 0) {
			break;
		}
	}

	exit(EXIT_SUCCESS);
}
