#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include "323.h"


int main(int argc, char *argv[])
{
	char input[MSG_SIZE];
	ssize_t received;
	char myfifo[PATH_MAX];

	char *home = getenv("HOME");
	snprintf(myfifo, PATH_MAX, "%s/%s", home, FIFONAME);
	int fp = open(myfifo, O_RDONLY);


	while (1) {
		if ((received = read(fp, input, MSG_SIZE)) == -1) {
			perror("read");
			exit(EXIT_FAILURE);
		}
		printf(">>> %s", input);
		if (strcmp(input, QUITUP) == 0)
			break;
	}

	if (close(fp) == -1)
		perror("close");

	if (unlink(myfifo) == -1)
		perror("unlink");

	exit(EXIT_SUCCESS);
}
