#include <stdlib.h>
#include <mqueue.h>
#include <stdio.h>
#include <string.h>
#include "323.h"

int main(int argc, char *argv[])
{
	struct mq_attr attr;
	char input[MSG_SIZE];
	mqd_t mqdes;
	ssize_t received;

	if ((mqdes = mq_open(QUEUE, O_RDONLY)) == -1) {
		perror("mq_open");
		exit(EXIT_FAILURE);
	}

	while (1) {
		if ((received = mq_receive(mqdes, input, sizeof(input), NULL)) == -1) {
			perror("mq_receive");
			exit(EXIT_FAILURE);
		}
		input[received] = '\0';
		if (strcmp(input, QUIT) == 0) {
			break;
		}
		printf(">>> %s", input);
	}

	exit(EXIT_SUCCESS);
}
