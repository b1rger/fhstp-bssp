//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "rawio.h"
#include <string.h>

#define DEFAULT_PORT 4323
#define MAX_LEN 1024

int lines = 0;
int fd;
char name[8];

// a thread that displays new messages
// every messages is displayed in a new line
void *prinfo(void *args)
{
	char buf[MAX_LEN];
	int outputline = 0;
	int rlen;
	while((rlen = read(fd, buf, sizeof(buf)-1)) > 0) {
		buf[rlen] = '\0';
		if (!strlen(buf))
			continue;
		writestr_raw(buf, 0, outputline);
		outputline++;
	}
	return NULL;
}

// display a prompt
void prompt() {
	writestr_raw("> ", 0, lines);
}

// generate a random nickname for the client
void randname() {
	srand(time(NULL));
	size_t l = 8;
	char charset[] = "0123456789"
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	while (l-- > 0) {
		size_t index = (double) rand() / RAND_MAX * (sizeof charset - 1);
		name[l] = charset[index];
	}
}

int main(int argc, char *argv[]) {
	pthread_t thrid;
	struct sockaddr_in serv_addr;
	char input[MAX_LEN];

	// if no nickname was give, generate one
	if (argc == 2) {
		snprintf(name, 8, "%s", argv[1]);
	} else {
		randname();
	}

	lines = get_lines();

	// open a socket to the server....
	fd = socket(AF_INET, SOCK_STREAM, 0);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(DEFAULT_PORT);
	inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

	// ... and connect to it
	if (connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		perror("ERROR connecting");
		exit(EXIT_FAILURE);
	}
	// send the client name to the server
	write(fd, name, strlen(name));

	// and create a thread that shows new messages
	pthread_create(&thrid, NULL, prinfo, NULL);
	clearscr();
	
	prompt();
	// as long as there are new messags, send them to the server
	while (gets_raw(input, MAX_LEN, 2, lines)!=NULL) {
		if (input[0] == 0)
			continue;
		write(fd, input, strlen(input));

		prompt();
	}

	exit(EXIT_SUCCESS);
}
