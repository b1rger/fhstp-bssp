//
// Copyright (C) 2018 Birger Schacht
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Authors: Birger Schacht <birger@rantanplan.org>
//
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_PORT 4323
#define MAX_LEN 1024
#define MAX_CLIENTS 16


// use a simple struct to store our client metadata
typedef struct {
	struct sockaddr_in addr;	/* Client remote address */
	int connfd;			/* Connection file descriptor */
	int uid;			/* Client unique identifier */
	char name[32];			/* Client name */
} client_t;

// and a basic array for all the clietns
client_t *clients[MAX_CLIENTS];


// a function to send a message to all the clients
void sendtoall(char *msg) {
	for (int i = 0; i < MAX_CLIENTS; i++) {
		if (clients[i]) {
			if (write(clients[i]->connfd, msg, strlen(msg)) == -1)
				perror("write");
		}
	}
}

// a function to add a client to the global client array
int addtoclientlist(client_t *c) {
	printf("Add %p to clientlist\n", c->name);
	for (int i = 0; i < MAX_CLIENTS; i++) {
		if (!clients[i]) {
			//printf("Adding client with fd %i to clientlist at pos %i\n", fd, i);
			clients[i] = c;
			return 1;
		}
	}
	printf("Sorry, no more clients allowed...");
	return 0;
}

// a function to remove a client from the global client array
void removefromclientlist(int uid) {
	printf("Removing %d from clientlist\n", uid);
	for (int i = 0; i < MAX_CLIENTS; i++) {
		if (clients[i]) {
			if (clients[i]->uid == uid) {
				clients[i] = NULL;
				return;
			}
		}
	}
}

/* Strip CRLF */
void strip_newline(char *s){
	while(*s != '\0'){
		if(*s == '\r' || *s == '\n'){
			*s = '\0';
		}
		s++;
	}
}

// the client handler thread
void *clhandler(void *args) {
	client_t *c = (client_t *)args;
	int rlen;

	char buf[MAX_LEN];
	char bufout[MAX_LEN*2];

	// as long as we get messages, we want to process them
	while ((rlen = read(c->connfd, buf, sizeof(buf)-1)) > 0 ) {
		buf[rlen] = '\0';
		strip_newline(buf);

		// ignore messages that are empty
		if (!strlen(buf))
			continue;

		// if the name of the client is not set yet
		// use the first message as the client name
		// and generate a join-message
		if (!strlen(c->name)) {
			sprintf(c->name, buf);
			snprintf(bufout, sizeof(bufout), "%s joined the chat", c->name);
		} else {
			snprintf(bufout, sizeof(bufout), "[%s] %s\r\n", c->name, buf);
		}

		// send the message to all the clients
		sendtoall(bufout);
	}
	

	// close the filedescriptor of this client,
	// remove the client from the client array
	// and free its memory
	close(c->connfd);
	removefromclientlist(c->uid);
	free(c);

	return NULL;
}


int main(int argc, char *argv[]) {
	int sock, port, fd; // claddlen sollte size_t sein
	unsigned int claddrlen;
	pthread_t thrid;
	struct sockaddr_in addr, claddr;
	static int uid = 0;

	// if we didn't get a port as an argument,
	// use the default port
	if (argc > 1) {
		sscanf(argv[1], "%i", &port);
	} else {
		port = DEFAULT_PORT;
	}

	// create socket, bind to the socket and listen on the socket
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;

	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	if ((bind(sock, (struct sockaddr *)(&addr), sizeof(addr))) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	if (listen(sock, 7) == -1) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	printf("Listening on port %i...\n", port);

	// listen for new connections...
	for(;;)
	{
		// if there is a new connection ...
		claddrlen = sizeof(claddr);
		if ((fd = accept(sock, (struct sockaddr *)(&claddr), &claddrlen)) == -1) {
			perror("accept");
			continue;
		}
		// create a client "object" for it
		client_t *c = (client_t *)malloc(sizeof(client_t));
		c->addr = claddr;
		c->connfd = fd;
		c->uid = uid++;
		// add the client to the list of clients
		if (addtoclientlist(c)) {
		// and pass the client to a thread that communicates with it
			if ((pthread_create(&thrid, NULL, clhandler,  (void*)c)) != 0) {
				perror("pthread_create");
				continue;
			}
		}
	}
	return 0;
}
